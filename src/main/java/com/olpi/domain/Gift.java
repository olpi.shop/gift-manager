package com.olpi.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class Gift {

    private static Long idCounter = 1L;

    private final Long id;
    private List<Sweet> sweets;

    public Gift() {
        this.id = idCounter++;
        this.sweets = new ArrayList<>();
    }

    public void addSweet(Sweet sweet) {
        if (!sweets.contains(sweet)) {
            sweets.add(sweet);
        } else {
            System.out.println("Candy already was added!");
        }
    }

    public Long getId() {
        return id;
    }

    public List<Sweet> getSweets() {
        return sweets;
    }

    public int getWeight() {
        return sweets.stream().mapToInt(Sweet::getWeight).sum();
    }

    public int getTotal() {
        return sweets.stream().mapToInt(Sweet::getPrice).sum();
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner("\n");
        joiner.add("**********************");
        joiner.add("Number of gift: " + id);
        joiner.add("Contains sweets: ");

        for (Sweet sweet : sweets) {
            joiner.add(sweet.toString());
        }
        joiner.add("--------------------------");
        joiner.add("Total cost: " + (double) getTotal() / 100 + "$");
        joiner.add("Weight: " + getWeight() + "gr");
        joiner.add("**********************\n");
        return joiner.toString();
    }

}
