package com.olpi.domain;

import java.util.Objects;

public class Sweet {

    private String name;
    private int weight;
    private int price;
    private int amountOfSugar;

    public Sweet(String name, int weight, int price, int amountOfSugar) {
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.amountOfSugar = amountOfSugar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmountOfSugar() {
        return amountOfSugar;
    }

    public void setAmountOfSugar(int amountOfSugar) {
        this.amountOfSugar = amountOfSugar;
    }

    @Override
    public String toString() {
        return "Name = \"" + name + "\", weight = " + weight + "gr, price = "
                + (double) price / 100 + "$, sugar = " + amountOfSugar + "gr.";
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, weight, price, amountOfSugar);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;

        Sweet other = (Sweet) obj;

        return (name.equals(other.name) && weight == other.weight
                && price == other.price
                && amountOfSugar == other.amountOfSugar);

    }

}
