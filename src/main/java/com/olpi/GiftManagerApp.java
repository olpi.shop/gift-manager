package com.olpi;

import java.util.List;

import com.olpi.domain.Gift;
import com.olpi.domain.Sweet;
import com.olpi.service.GiftService;

public class GiftManagerApp {

    public static void main(String[] args) {

        GiftService giftService = new GiftService();

        Sweet snikers = new Sweet("Snikers", 80, 200, 14);
        Sweet mars = new Sweet("Mars", 100, 225, 32);
        Sweet bounty = new Sweet("Bounty", 90, 195, 25);
        Sweet superContic = new Sweet("Super Contic", 150, 133, 29);

        Gift gift = new Gift();
        gift.addSweet(superContic);
        gift.addSweet(bounty);
        gift.addSweet(mars);
        gift.addSweet(snikers);

        gift.addSweet(superContic);

        System.out.println("\nGift:\n" + gift.toString());

        System.out.println("Sorted by sugar:");
        giftService.sortSweetsByAmountOfSugar(gift);
        System.out.println(gift.toString());

        System.out.println("Sorted by price:");
        giftService.sortSweetsByPrice(gift);
        System.out.println(gift.toString());

        System.out.println("Sorted by weight:");
        giftService.sortSweetsByWeight(gift);
        System.out.println(gift.toString());

        System.out.println(
                "Candies, which contains from 18 to 30 grams of sugar:");

        List<Sweet> sweets = giftService.findSweetsByAmountOfSugar(gift, 18,
                30);
        for (Sweet sweet : sweets) {
            System.out.println(sweet.toString());
        }

    }
}
