package com.olpi.service;

import java.util.List;
import java.util.stream.Collectors;

import com.olpi.domain.Gift;
import com.olpi.domain.Sweet;

public class GiftService {

    public void sortSweetsByWeight(Gift gift) {
        gift.getSweets().sort((a, b) -> a.getWeight() - b.getWeight());
    }

    public void sortSweetsByAmountOfSugar(Gift gift) {
        gift.getSweets()
                .sort((a, b) -> a.getAmountOfSugar() - b.getAmountOfSugar());
    }

    public void sortSweetsByPrice(Gift gift) {
        gift.getSweets().sort((a, b) -> a.getPrice() - b.getPrice());
    }

    public List<Sweet> findSweetsByAmountOfSugar(Gift gift, int minAmount,
            int maxAmount) {
        return gift.getSweets().stream()
                .filter(a -> a.getAmountOfSugar() >= minAmount
                        && a.getAmountOfSugar() <= maxAmount)
                .collect(Collectors.toList());
    }

}
